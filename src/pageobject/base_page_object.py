class BasePageObject:
    
    def __init__(self, browser) -> None:
        self._browser = browser
    
    def find_element_by_xpath(self, xpath):
        return self.find_element_by_xpath(xpath)
from src.pageobject.base_page_object import BasePageObject
from selenium.webdriver.common.keys import Keys


class SearchPage(BasePageObject):

    def __init__(self, browser) -> None:
        super().__init__(browser)
        self._browser.get("https://www.baidu.com/")

    def get_search_keyword(self) -> str:
        return self._browser.find_element_by_xpath('//*[@id="kw"]').get_attribute('value')

    def search(self, search_keyword: str):
        search_input_box = self._browser.find_element_by_xpath('//*[@id="kw"]')
        search_input_box.send_keys(search_keyword)
        search_input_box.send_keys(Keys.ENTER)

import time
from src.pageobject.search_page import SearchPage


class TestSearchPage():

    def test_search(self, browser):
        search_page = SearchPage(browser)
        search_keyword = '益迪'
        search_page.search(search_keyword)
        time.sleep(0.2)
        assert search_keyword == search_page.get_search_keyword()

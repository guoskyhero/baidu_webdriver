import time
import pytest
from selenium import webdriver
import sys
import os

sys.path.append(
    (os.path.abspath(os.path.join(os.path.dirname(__file__), '../'))))


@pytest.fixture()
def browser():
    chrome_options = webdriver.ChromeOptions()
    browser = webdriver.Chrome(
        options=chrome_options
    )
    yield browser
    time.sleep(5)
    browser.quit()

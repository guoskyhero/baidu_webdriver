from selenium import webdriver
from selenium.webdriver.common.keys import Keys
 
chrome_options = webdriver.ChromeOptions()
driver = webdriver.Chrome(
    options=chrome_options
)
driver.get("https://www.baidu.com/")

search_input_box = driver.find_element_by_xpath('//*[@id="kw"]')
search_input_box.send_keys('益迪')
search_input_box.send_keys(Keys.ENTER)
print(search_input_box.get_attribute('value'))

search_submit_button = driver.find_element_by_xpath('//*[@id="su"]')
print(search_submit_button.get_attribute('value'))

# driver.quit()
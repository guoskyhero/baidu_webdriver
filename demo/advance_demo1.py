import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
 
chrome_options = webdriver.ChromeOptions()
driver = webdriver.Chrome(
    options=chrome_options
)
driver.get("https://www.baidu.com/")

login_button = driver.find_element_by_xpath('//*[@id="s-top-loginbtn"]')
login_button.click()

time.sleep(1)
login_form = driver.find_element_by_xpath('//*[@id="passport-login-pop-dialog"]/div/div/div')
username_login_entry = login_form.find_element_by_xpath('div[3]/p[2]')
username_login_entry.click()

username_input_box = login_form.find_element_by_xpath('div/form/p[5]/input[2]')
username_input_box.send_keys('测试')
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
 
chrome_options = webdriver.ChromeOptions()
driver = webdriver.Chrome(
    options=chrome_options
)
driver.get("https://www.baidu.com/")

search_word = '益迪'

search_input_box = driver.find_element_by_xpath('//*[@id="kw"]')
search_input_box.send_keys(search_word)
search_input_box.send_keys(Keys.ENTER)

# print(search_input_box.get_attribute('value'))
# assert search_word == search_input_box.get_attribute('value')

assert '这是错的' == search_input_box.get_attribute('value'), f'实际结果 ' \
    f'{search_input_box.get_attribute("value")}'